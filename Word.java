package dictionary;

/**
 * Word la chuong trinh mo ta thuoc tinh cua 1 tu trong tu dien
 * @author Trinh Duc Duy, Le Van Trung Hieu
 * @version 1.0
 * @since 2018-10-01
 */

public class Word {
    private String word_target;
    private String word_explain;
    
    Word() {
        word_target = "";
        word_explain = "";
    }
    
    Word(String target,String explain) {
        word_target = target;
        word_explain = explain;
    }
    
    /*
    Cac phuong thuc get, set cac thuoc tinh tuong ung cua lop Word
    */
    
    public void setTarget(String target) {
        word_target = target;
    }
    public String getTarget() {
        return word_target;
    }
    
    public void setExplain(String explain) {
        word_explain = explain;
    }
    public String getExplain() {
        return word_explain;
    }
}

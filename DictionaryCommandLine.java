package dictionary;

/**
 * DictionaryCommandLine la chuong trinhnhap tu, in toan bo cac tu trong cmd
 * @author Trinh Duc Duy, Le Van Trung Hieu
 * @version 1.0
 * @since 2018-10-01
 */

import java.util.ArrayList;

public class DictionaryCommandLine {
    
    /**
    * showAllWords la 1 phuong thuc in ra toan bo tu trong mang
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
    
    public void showAllWords(ArrayList<Word> dict) {
        
        /*
        Tinh chinh viec in tu ra man hinh
        Phu hop voi so luong khoang 1 den 999.999 tu
        Phu hop voi tu tieng Anh khong qua 17 chu cai
        */
        
        System.out.println("No"+"     "+"|English"+"          "+"|Vietnamese");
        for(int i=0;i<dict.size();i++) {
            
            System.out.print((i+1));
            
            String s;
            s = String.valueOf((i+1));
            
            for(int j=0;j<(7-s.length());j++) {
                System.out.print(" ");
            }
            
            System.out.print("|"+dict.get(i).getTarget());
            
            for(int k=0;k<(17-dict.get(i).getTarget().length());k++) {
                System.out.print(" ");
            }
            
            System.out.println("|"+dict.get(i).getExplain());  
        }
    }
    
    /**
    * dictionaryBasic la 1 phuong thuc nhap tu va in tu
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
    
    public void dictionaryBasic(ArrayList<Word> dict) {
        
        DictionaryManagement word = new DictionaryManagement(); //khoi tao doi tuong lop DictionaryManagement
        
        word.insertFromCommandline(dict); //chay ham insertFromCommandline tu lop DictionaryManagement
        
        showAllWords(dict); //in ra toan bo cac tu cua mang dict
        
    }
    
}

package dictionary;

/**
 * DictionaryManagement la chuong trinh gom cac thao tac nhap tu ban phim
 * @author Trinh Duc Duy, Le Van Trung Hieu
 * @version 1.0
 * @since 2018-10-01
 */

import java.util.Scanner;
import java.util.ArrayList;

public class DictionaryManagement {
    
    /**
    * insertFromCommandline la 1 phuong thuc nhap du lieu tu va giai thich
    * @param dict the hien mang luu tru cac doi tuong thuoc class Word
    */
	 
    public void insertFromCommandline(ArrayList<Word> dict) {
        
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhap so luong tu: "); //in ra yeu cau nhap so luong tu
        int n = scan.nextInt(); //nhap so luong tu
        scan.nextLine();
        
        for(int i=0;i<n;i++) {
            
            System.out.print("Nhap tu tieng Anh: "); //in ra yeu cau nhap tu tieng Anh
            String target = scan.nextLine(); //nhap tu tieng Anh
            scan.nextLine();
            System.out.print("Nhap giai thich tieng Viet: "); //in ra yeu cau nhap giai thich tieng Viet
            String explain = scan.nextLine(); //nhap giai thich
            
            Word newWord = new Word(target, explain); //khoi tao doi tuong lop Word voi hai thuoc tinh nhap o tren
            
            dict.add(newWord); //them doi tuong vua tao vao mang
        }
    }
}

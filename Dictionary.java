package dictionary;

/**
 * Dictionary la 1 ung dung cho phep nhap tu va in ra man hinh
 * @author Trinh Duc Duy
 * @version 1.0
 * @since 2018-10-06
 */

import java.util.ArrayList;


public class Dictionary {

    /**
     * Sau day lam ham main de thuc thi viec tao mang, nhap lieu, in ket qua
     * @param args khong su dung
     * @return ham main thi khong tra ve gia tri nao ca
     */
    
    public static void main(String[] args) {
        
        ArrayList<Word> dict = new ArrayList<>(); //khoi tao mang dict luu tru Word
        
        DictionaryCommandLine run = new DictionaryCommandLine(); //khoi tao doi tuong thuov lop DictionaryCommandLine
        
        run.dictionaryBasic(dict); //chay ham nhap tu va in ket qua ra man hinh
        
    }
    
}
